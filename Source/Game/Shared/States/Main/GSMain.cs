﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Logic.StateSystem;

namespace ABitzProject.Shared.States.Main
{
    internal class GSMain : State
    {
        private ICamera _GameCamera;
        private Random _RND = new Random();
        private TimeSpan _TimeTillBird= TimeSpan.Zero;
        public GSMain()
        {
            OnEnter += GSMain_OnEnter;
        }

        private void GSMain_OnEnter(State obj)
        {
            _GameCamera = Injector.GetSingleton<IGraphicsService>().DefaultCanvas.PrimaryCamera;}

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {

        }
    }
}
