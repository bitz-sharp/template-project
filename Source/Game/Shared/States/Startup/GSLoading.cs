﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Drawables;
using Bitz.Modules.Core.Logic.StateSystem;
using OpenTK;

namespace ABitzProject.Windows.States.Startup
{
    class GSLoading : State
    {
        private Task _LoadingTask;
        private Sprite _LoadingText;

        public GSLoading()
        {
            OnEnter += Loading_OnEnter;
            OnExit += GSLoading_OnExit;
        }

        private void GSLoading_OnExit()
        {
            _LoadingText.Dispose();
            _LoadingText = null;
        }

        private void Loading_OnEnter(State obj)
        {
            IAssetsService assetService = Injector.GetSingleton<IAssetsService>();
            IGraphicsService graphicsSerivce = Injector.GetSingleton<IGraphicsService>();
           _LoadingText = new Sprite
            {
                Texture = assetService.GetTexture("Graphics\\Initial\\LoadingText.png"),
                Visible = true,
                Scale = Vector2.One*0.2f
            };

            Vector2 screenCenter = (graphicsSerivce.DefaultCanvas.PrimaryCamera as ICamera2D).Position + (graphicsSerivce.DefaultCanvas.PrimaryCamera as ICamera2D).Size * 0.5f;

            _LoadingText.Position = screenCenter -= (_LoadingText.Scale* _LoadingText.Texture.Size) * 0.5f;

            _LoadingTask = new Task(() =>
            {
                assetService.BuildPack("Content/Graphics/Fonts.pack");
                Thread.Sleep(3000); //this is just to show the loading screen briefly, no actual need for this
                NextState=new GSIntro();
            });
            _LoadingTask.Start();
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }
    }
}