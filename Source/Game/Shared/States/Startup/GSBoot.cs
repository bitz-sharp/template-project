﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Logic.StateSystem;
using OpenTK;

namespace ABitzProject.Windows.States.Startup
{
    class GSBoot : State
    {
        private IGraphicsService _GraphicsService;
        public GSBoot()
        {
            OnEnter += Boot_OnEnter;
            OnExit += Boot_OnExit;
        }

        private void Boot_OnExit()
        {
            _GraphicsService = null;
        }

        private void Boot_OnEnter(State obj)
        {
            _GraphicsService = Injector.GetSingleton<IGraphicsService>();
            var _AssetService = Injector.GetSingleton<IAssetsService>();
            _AssetService.BuildPack("Content/Graphics/Initial.pack");
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            if (_GraphicsService.DefaultCanvas.PrimaryCamera.Size != Vector2.Zero && Injector.GetSingleton<IAssetsService>().GetTexture("Graphics\\Initial\\LoadingText.png")!=null) NextState = new GSLoading(); //might be worth changing this into a graphics manager . is ready call
        }
    }
}