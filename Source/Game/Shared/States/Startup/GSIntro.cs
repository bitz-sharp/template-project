﻿using System;
using System.Threading.Tasks;
using ABitzProject.Shared.States.Main;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Drawables;
using Bitz.Modules.Core.Logic.StateSystem;
using Bitz.Modules.Core.Logic.Tween;
using OpenTK;

namespace ABitzProject.Windows.States.Startup
{
    class GSIntro : State
    {
        private Task _IntroTask;
        private Sprite[] _RuneSprites = new Sprite[4];
        private Vector2 _ScreenCenter;
        private Tweener _Tweener;

        public GSIntro()
        {
            OnEnter += Intro_OnEnter;
            OnExit += GSIntro_OnExit;
        }

        private void GSIntro_OnExit()
        {

        }

        private void Intro_OnEnter(State obj)
        {
            IAssetsService assetService = Injector.GetSingleton<IAssetsService>();
            IGraphicsService graphicsSerivce = Injector.GetSingleton<IGraphicsService>();

            _ScreenCenter = (graphicsSerivce.DefaultCanvas.PrimaryCamera as ICamera2D).Position + (graphicsSerivce.DefaultCanvas.PrimaryCamera as ICamera2D).Size * 0.5f;

            _RuneSprites[0] = new Sprite
            {
                Texture = assetService.GetTexture("Graphics\\Initial\\runeBlack_slabOutline_001.png"),
                Visible = true,
                Position = new Vector2(_ScreenCenter.X - (56 * 1.5f), -500)
            };
            _RuneSprites[1] = new Sprite
            {
                Texture = assetService.GetTexture("Graphics\\Initial\\runeBlack_slabOutline_008.png"),
                Visible = true,
                Position = new Vector2(_ScreenCenter.X - (56 * 0.5f), -700)
            };
            _RuneSprites[2] = new Sprite
            {
                Texture = assetService.GetTexture("Graphics\\Initial\\runeBlack_slabOutline_019.png"),
                Visible = true,
                Position = new Vector2(_ScreenCenter.X + (56 * 0.5f), -800)
            };
            _RuneSprites[3] = new Sprite
            {
                Texture = assetService.GetTexture("Graphics\\Initial\\runeBlack_slabOutline_031.png"),
                Visible = true,
                Position = new Vector2(_ScreenCenter.X + (56 * 1.5f), -900)

            };
            _Tweener = new Tweener();
            _Tweener.Tween(_RuneSprites[0], new { PosY = _ScreenCenter.Y - (93 / 2.0f) }, 1);
            _Tweener.Tween(_RuneSprites[1], new { PosY = _ScreenCenter.Y - (93 / 2.0f) }, 1);
            _Tweener.Tween(_RuneSprites[2], new { PosY = _ScreenCenter.Y - (93 / 2.0f) }, 1);
            _Tweener.Tween(_RuneSprites[3], new { PosY = _ScreenCenter.Y - (93 / 2.0f) }, 1)
                .OnComplete(() =>
                {
                    new Bitz.Modules.Core.Logic.Triggers.CountdownTrigger(TimeSpan.FromSeconds(1.3), (lt) => { NextState = new GSMain(); });
                });
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            _Tweener.Update((float)timeSinceLastUpdate.TotalSeconds);
        }
    }
}