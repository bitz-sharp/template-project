using Android.App;
using Android.Content.PM;

namespace ABitzProject.Android
{
    [Activity(Label = "Avoid", Theme = "@android:style/Theme.Black.NoTitleBar.Fullscreen", ScreenOrientation = ScreenOrientation.SensorLandscape, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize, MainLauncher = true)]
    class Activity : Bitz.Modules.Platform.Android.Platform.BitzActivity
    {
        public Activity() : base(typeof(GameInstance))
        {

        }
    }
}