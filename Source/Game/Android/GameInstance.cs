﻿using ABitzProject.Windows.States.Startup;
using Bitz.Modules.Core.Debug;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Graphics;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Input;
using Bitz.Modules.Core.Logic.StateSystem;
using OpenTK;

namespace ABitzProject.Android
{
    public class GameInstance : Bitz.Modules.Platform.Android.GameInstance
    {

        public GameInstance() : base(new ScreenLayout(new Vector2(1280, 600
        )), loggerSeverity: LogSeverity.VERBOSE | LogSeverity.ERROR | LogSeverity.CRITICAL | LogSeverity.WARNING)
        {
            Injector.RegisterMapping<IDebugService>(typeof(DebugService), true);

        }


        protected override void OnInit()
        {
            Injector.GetSingleton<IGraphicsService>().DefaultCanvas.PrimaryCamera.SetModeTargetWidth(1280);
            Injector.GetSingleton<IGraphicsService>().FPS = 60;
            Injector.GetSingleton<IStateService>().EnterState(new GSBoot());

            Injector.GetSingleton<IInputService>().DebugMode = true;
            Injector.GetSingleton<IGraphicsService>().DebugMode = true;

        }

        protected override void OnExit()
        {
        }
    }
}