﻿using System;
using ABitzProject.Windows.States.Startup;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Graphics;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Logic.StateSystem;
using OpenTK;

namespace ABitzProject.Windows
{
    public class GameInstance : Bitz.Modules.Platform.Web.GameInstance
    {
        public GameInstance() : base(new ScreenLayout(new Vector2(1920, 1080
        )), loggerSeverity: LogSeverity.VERBOSE | LogSeverity.ERROR | LogSeverity.CRITICAL | LogSeverity.WARNING)
        {
        }


        protected override void OnInit()
        {
            Injector.GetSingleton<IGraphicsService>().DefaultCanvas.PrimaryCamera.SetModeTargetWidth(1280);
            Injector.GetSingleton<IGraphicsService>().FPS = 60;
            Injector.GetSingleton<IStateService>().EnterState(new GSBoot());

        }

        protected override void OnExit()
        {
        }

        static void Main(String[] args)
        {
            new GameInstance().Run();
        }
    }
}